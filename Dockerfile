FROM openjdk:8-jre-alpine

# Copying the jar file
COPY target/ip-config-0.0.1-SNAPSHOT.jar /ip-config-0.0.1-SNAPSHOT.jar

# launching the jar
CMD ["sh", "-c", "/usr/bin/java $JAVA_OPTS -jar /ip-config-0.0.1-SNAPSHOT.jar"]
